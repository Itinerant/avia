package com.avia.sale.avia;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class FindTicketActivity extends AppCompatActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.find_ticket);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.find_ticket_btn_search:
                Intent intent = new Intent(this, OptionsFoundActivity.class);
                startActivity(intent);
                break;
        }
    }
}
